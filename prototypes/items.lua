-------------------------------------------------------------------------
-- Raw resources stack extend
-------------------------------------------------------------------------  
data.raw['item']['coal'].stack_size = 200;
data.raw['item']['copper-ore'].stack_size = 200;
data.raw['item']['iron-ore'].stack_size = 200;
data.raw['item']['stone'].stack_size = 200;
data.raw['item']['uranium-ore'].stack_size = 200;
-------------------------------------------------------------------------
-- Belts stack extend
-------------------------------------------------------------------------
data.raw['item']['transport-belt'].stack_size = 200;
data.raw['item']['fast-transport-belt'].stack_size = 200;
data.raw['item']['express-transport-belt'].stack_size = 200;

data.raw['item']['underground-belt'].stack_size = 100;
data.raw['item']['fast-underground-belt'].stack_size = 100;
data.raw['item']['express-underground-belt'].stack_size = 100;

data.raw['item']['splitter'].stack_size = 100;
data.raw['item']['fast-splitter'].stack_size = 100;
data.raw['item']['express-splitter'].stack_size = 100;
-------------------------------------------------------------------------
-- Inserters
-------------------------------------------------------------------------
data.raw['item']['inserter'].stack_size = 100;
data.raw['item']['long-handed-inserter'].stack_size = 100;
data.raw['item']['fast-inserter'].stack_size = 100;
data.raw['item']['filter-inserter'].stack_size = 100;
data.raw['item']['stack-inserter'].stack_size = 100;
data.raw['item']['stack-filter-inserter'].stack_size = 100;
-------------------------------------------------------------------------
-- Electric pole
-------------------------------------------------------------------------
data.raw['electric-pole']['big-electric-pole'].maximum_wire_distance = 32;
data.raw['electric-pole']['medium-electric-pole'].supply_area_distance = 4.5;