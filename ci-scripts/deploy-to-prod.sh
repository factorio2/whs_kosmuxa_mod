#!/bin/sh

sudo systemctl stop factorio.service
sudo rm -rf /opt/factorio/mods/kosmuxa-extend_*
unzip kosmuxa-extend_* -d /opt/factorio/mods/
sudo systemctl start factorio.service