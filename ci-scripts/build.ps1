$info = Get-Content "info.json" -raw | ConvertFrom-Json
$targetPath = "kosmuxa-extend_" + $info.version

New-Item -Path $targetPath -ItemType directory

Copy-Item -Path .\graphics -Destination $targetPath -Recurse -Verbose
Copy-Item -Path .\locale -Destination $targetPath -Recurse -Verbose
Copy-Item -Path .\lib -Destination $targetPath -Recurse -Verbose
Copy-Item -Path .\prototypes -Destination $targetPath -Recurse -Verbose
Copy-Item -Path info.json -Destination $targetPath -Verbose
Copy-Item -Path data.lua -Destination $targetPath -Verbose
Copy-Item -Path LICENSE -Destination $targetPath -Verbose


$compress = @{
	Path = $targetPath
	CompressionLevel = "Fastest"
	DestinationPath = $targetPath + ".zip"
}
Compress-Archive @compress -Verbose
