Write-Host "Generating version number..."

$gitLatestTagedCommitSHA = $(git rev-list HEAD --max-count=1)
$gitLatestTag = $(git describe --tags $gitLatestTagedCommitSHA)
$LatestTag = $gitLatestTag.Substring(0,3)

$revCount = $(git rev-list --count $gitLatestTagedCommitSHA)
$revCountTag = $(git rev-list --count $LatestTag)

$build = $revCount-$revCountTag
$versionNumber = "$LatestTag"+"."+"$build"

Write-Host "Current version is:" $versionNumber

$info = Get-Content "info.json" -raw | ConvertFrom-Json
$info.version = $versionNumber
$info | ConvertTo-Json -depth 32 | set-content "info.json"

Get-Content "info.json"
