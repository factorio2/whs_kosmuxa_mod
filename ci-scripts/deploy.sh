#!/bin/sh

sudo systemctl stop factorio-test.service
sudo rm -rf /opt/factorio-test/mods/kosmuxa-extend_*
unzip kosmuxa-extend_* -d /opt/factorio-test/mods/
sudo systemctl start factorio-test.service