Write-Host "Get actual version and paths"

$info = Get-Content "info.json" -raw | ConvertFrom-Json
$dir = "kosmuxa-extend_" + $info.version + ".zip"
$source = ".\kosmuxa-extend_" + $info.version + ".zip"
$target = "C:\Users\kostm\AppData\Roaming\Factorio\mods\"

Write-Host "Source: "$source
Write-Host "Target: "$target

Remove-Item -Path "C:\Users\kostm\AppData\Roaming\Factorio\mods\kosmuxa-extend*" -Recurse -Force
Expand-Archive -Path $source -DestinationPath "C:\Users\kostm\AppData\Roaming\Factorio\mods\"
